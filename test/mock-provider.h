#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define MOCK_TYPE_PROVIDER (mock_provider_get_type())

G_DECLARE_FINAL_TYPE (MockProvider, mock_provider, MOCK, PROVIDER, GObject)

MockProvider *mock_provider_new (void);
gboolean mock_provider_is_loaded (MockProvider *self);

G_END_DECLS
