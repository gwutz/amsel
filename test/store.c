#include "amsel-store.h"
#include "model/amsel-entry.h"
#include <glib.h>

static gboolean
traverse_func (gpointer key,
               gpointer value,
               gpointer user_data)
{
  gchar buffer[100];
  GDate *date = (GDate *)key;

  g_date_strftime (buffer, 100, "%d.%m.%Y", date);
  g_print ("%s\n", buffer);

  return FALSE;
}

static void
test_store (void)
{
  AmselStore *store = amsel_store_new ();

  AmselEntry *entry = amsel_entry_new ();
  amsel_entry_set_updated (entry, "2018-05-20T12:00:00+00:00");

  amsel_store_append (store, entry);

  g_object_unref (entry);
  entry = amsel_entry_new ();
  amsel_entry_set_updated (entry, "2018-05-20T13:00:00+00:00");

  amsel_store_append (store, entry);
  g_object_unref (entry);

  entry = amsel_entry_new ();
  amsel_entry_set_updated (entry, "2018-05-21T13:00:00+00:00");

  amsel_store_append (store, entry);
  g_object_unref (entry);

  amsel_store_foreach (store, traverse_func);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/amsel/store", test_store);

  return g_test_run ();
}
