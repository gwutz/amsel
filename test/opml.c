#include <glib.h>
#include <opml/amsel-opml.h>

gchar *opmlfile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
"<opml version=\"1.0\">"
    "<head>"
        "<title>Günther subscriptions in feedly Cloud</title>"
    "</head>"
    "<body>"
        "<outline text=\"Linux\" title=\"Linux\">"
            "<outline type=\"rss\" text=\"OMG! Ubuntu!\" title=\"OMG! Ubuntu!\" xmlUrl=\"http://feeds.feedburner.com/d0od\" htmlUrl=\"https://www.omgubuntu.co.uk\"/>"
            "<outline type=\"rss\" text=\"Planet KDE\" title=\"Planet KDE\" xmlUrl=\"http://planetkde.org/rss20.xml\" htmlUrl=\"http://planetKDE.org/\"/>"
            "<outline type=\"rss\" text=\"Planet GNOME\" title=\"Planet GNOME\" xmlUrl=\"http://planet.gnome.org/rss20.xml\" htmlUrl=\"http://planet.gnome.org/\"/>"
        "</outline>"
    "</body>"
"</opml>";

void
test_opml (void)
{
  gchar *urls_expected[] = {
    "http://feeds.feedburner.com/d0od",
    "http://planetkde.org/rss20.xml",
    "http://planet.gnome.org/rss20.xml"
  };
  AmselOpml *opmlparser = amsel_opml_new ();

  GList *urls = amsel_opml_parse (opmlparser, opmlfile);

  gint i = 0;
  for (GList *cur = urls; cur; cur = g_list_next (cur), i++)
    {
      g_assert_cmpstr (cur->data, ==, urls_expected[i]);
    }

  g_list_free_full (urls, g_free);

  g_object_unref (opmlparser);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/amsel/providermanager", test_opml);

  return g_test_run ();
}
