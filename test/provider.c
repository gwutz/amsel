#include <glib.h>
#include <amsel-feed-provider-manager.h>
#include <mock-provider.h>

void
test_providermanager (void)
{
  AmselFeedProviderManager *manager = amsel_feed_provider_manager_new ();

  // ensure the manager is empty
  GList *providerlist = amsel_feed_provider_manager_get_provider_list (manager);
  g_assert_cmpint (g_list_length (providerlist), ==, 0);

  g_list_free (providerlist);

  MockProvider *provider = mock_provider_new ();
  amsel_feed_provider_manager_add_provider (manager, "mock", AMSEL_FEED_PROVIDER (provider));

  providerlist = amsel_feed_provider_manager_get_provider_list (manager);
  g_assert_cmpint (g_list_length (providerlist), ==, 1);

  // ensure the provider is loaded
  g_assert_true (mock_provider_is_loaded (provider));

  g_list_free (providerlist);
  g_object_unref (manager);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/amsel/providermanager", test_providermanager);

  return g_test_run ();
}
