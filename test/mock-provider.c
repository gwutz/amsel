#include "mock-provider.h"
#include <amsel-feed-provider.h>

struct _MockProvider
{
  GObject parent_instance;

  gboolean is_loaded;
};

static void amsel_feed_provider_iface_init (AmselFeedProviderInterface *iface);
G_DEFINE_TYPE_WITH_CODE (MockProvider, mock_provider, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (AMSEL_TYPE_FEED_PROVIDER, amsel_feed_provider_iface_init))

MockProvider *
mock_provider_new (void)
{
  return g_object_new (MOCK_TYPE_PROVIDER, NULL);
}

static void
mock_provider_finalize (GObject *object)
{
  /* MockProvider *self = (MockProvider *)object; */

  G_OBJECT_CLASS (mock_provider_parent_class)->finalize (object);
}

static void
mock_provider_class_init (MockProviderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mock_provider_finalize;
}

static void
mock_provider_init (MockProvider *self)
{
  self->is_loaded = FALSE;
}

static void
mock_provider_load (AmselFeedProvider *provider)
{
  MockProvider *self = MOCK_PROVIDER (provider);

  self->is_loaded = TRUE;
}

static void
amsel_feed_provider_iface_init (AmselFeedProviderInterface *iface)
{
  iface->load = mock_provider_load;
}

gboolean
mock_provider_is_loaded (MockProvider *self)
{
  return self->is_loaded;
}
