option('enable_tracing', type: 'boolean', value: false, description: 'Enable tracing of internals for troubleshooting Amsel')
option('with_feedly', type: 'boolean', value: false, description: 'Enable feedly plugin')
option('with_gtkdoc', type: 'boolean', value: false, description: 'Enable generation of libamsel reference')