# amsel-sqlite.py
#
# Copyright 2018 Guenther Wagner <info@gunibert.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#!/usr/bin/env python2
from gi.repository import GLib
from gi.repository import GObject
from gi.repository import Amsel
import sqlite3

class AmselSqlite(GObject.Object, Amsel.FeedPersistence):

    def __init__(self):
        self.db = sqlite3.connect ('/home/gunibert/amsel.db')
        self.create_tables()

    def do_store(self, channel):
        Amsel.debug(__name__, "Storing channel and items")
        new_items = []
        c = self.db.cursor()
        try:
            Amsel.debug(__name__, "store channel: " + channel.get_title())
            c.execute('''INSERT INTO channels VALUES (?, ?, ?, ?)''',
                      (channel.get_id(),
                       channel.get_plugin(),
                       channel.get_title(),
                       channel.get_source()))
            self.db.commit()
        except sqlite3.Error as err:
            Amsel.debug(__name__, str(err))

        for entry in channel.get_entries():
            try:
                Amsel.debug(__name__, "store entry: " + entry.get_title())
                c.execute('''INSERT INTO entries VALUES (?, ?, ?, ?, ?, ?, ?, ?)''',
                          (entry.get_id(),
                          entry.get_title(),
                          entry.get_content(),
                          entry.get_updated().format("%Y-%m-%dT%H:%M:%S%:z"),
                          entry.get_link(),
                          entry.get_preview_image(),
                          entry.get_read(),
                          channel.get_id()
                          ))
                new_items.append(entry)
            except sqlite3.Error as err:
                Amsel.debug(__name__, str(err))

        self.db.commit()

        Amsel.debug(__name__, "New Items: " + str(len(new_items)))
        return new_items

    def do_get_channels (self):
        Amsel.debug(__name__, "Start select of channels")
        c = self.db.cursor()
        channels = []

        for row in c.execute('''SELECT id, plugin, title, source FROM channels'''):
            channel = Amsel.Channel.new(row[1])
            channel.set_id(row[0])
            channel.set_title(row[2])
            channel.set_source(row[3])
            channels.append(channel)

        return channels

    def do_n_channels (self):
        c = self.db.cursor()
        Amsel.debug(__name__, "start counting...")
        for row in c.execute('''SELECT COUNT(id) FROM channels'''):
            n_channels = row[0]
            Amsel.debug(__name__, "Counted Channels: " + str(n_channels))
            return row[0]
        return 0

    def do_get_channels_for_plugin (self, pluginid):
        Amsel.debug(__name__, "Get Channels from plugin " + pluginid)
        c = self.db.cursor()
        channels = []

        for row in c.execute('''SELECT id, plugin, title, source FROM channels WHERE plugin =:id''', {"id": pluginid}):
            channel = Amsel.Channel.new(row[1])
            channel.set_id(row[0])
            channel.set_title(row[2])
            channel.set_source(row[3])
            channels.append(channel)

        return channels


    def do_mark_read (self, entry, read):
        c = self.db.cursor()
        if read == True:
            read = 1
        else:
            read = 0
        try:
            c.execute ('''UPDATE entries SET read=:read WHERE id=:id''', {"read": read, "id": entry.get_id ()})
        except sqlite3.Error as err:
            Amsel.debug(__name__, str(err))
        self.db.commit()


    def do_get_entries_for_channel(self, channel):
        Amsel.debug(__name__, "Start select of items")
        c = self.db.cursor()
        entries = []

        cid = channel.get_id()
        for row in c.execute('''SELECT id, title, content, updated, link, preview_image, read, channel_id FROM entries WHERE channel_id=:id''', {"id": cid}):
            entry = Amsel.Entry()
            entry.set_id (row[0])
            entry.set_title (row[1])
            entry.set_content (row[2])
            entry.set_updated (row[3])
            entry.set_link (row[4])
            if row[5] is not None:
                entry.set_preview_image (row[5])
            entry.set_read (row[6])
            entries.append(entry)

        return entries

    def create_tables(self):
        c = self.db.cursor()
        try:
            c.execute('''CREATE TABLE IF NOT EXISTS channels (
                id text PRIMARY KEY,
                plugin text,
                title text,
                source text
            )''')

            self.db.commit()
            c.execute('''CREATE TABLE IF NOT EXISTS entries (
                id text PRIMARY KEY,
                title text,
                content text,
                updated text,
                link text,
                preview_image text,
                read integer,
                channel_id text,
                FOREIGN KEY(channel_id) REFERENCES channels(id)
            )''')

            self.db.commit()
        except sqlite3.Error as err:
            print(err)