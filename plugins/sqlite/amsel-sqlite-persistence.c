/* amsel-sqlite-persistence.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */
#define G_LOG_DOMAIN "amsel-sqlite-persistence"

#include "amsel-sqlite-persistence.h"
#include "amsel-feed-persistence.h"
#include "model/amsel-channel.h"
#include <sqlite3.h>

#define SQL_CMP(column) g_strcmp0 (column_name, column) == 0
#define TEXT (const gchar *)

struct _AmselSqlitePersistence
{
  GObject parent_instance;

  sqlite3 *db;
};

static void persistence_iface_init (AmselFeedPersistenceInterface *iface);
G_DEFINE_TYPE_WITH_CODE (AmselSqlitePersistence, amsel_sqlite_persistence, G_TYPE_OBJECT,
                           G_IMPLEMENT_INTERFACE (AMSEL_TYPE_FEED_PERSISTENCE, persistence_iface_init)
                         )

static GList *
amsel_sqlite_persistence_store (AmselFeedPersistence *persistence,
                                AmselChannel         *channel)
{
  g_return_val_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (persistence), NULL);

  GList *new_items = NULL;
  gint rc;
  sqlite3_stmt *stmt;
  AmselSqlitePersistence *self = AMSEL_SQLITE_PERSISTENCE (persistence);

  g_debug ("%s", "Start storing channel");
  sqlite3_exec (self->db, "BEGIN TRANSACTION;", NULL, NULL, NULL);

  rc = sqlite3_prepare_v2 (self->db, "INSERT INTO channels VALUES (?, ?, ?, ?)", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  sqlite3_bind_text (stmt, 1, amsel_channel_get_id (channel), -1, NULL);
  sqlite3_bind_text (stmt, 2, amsel_channel_get_plugin (channel), -1, NULL);
  sqlite3_bind_text (stmt, 3, amsel_channel_get_title (channel), -1, NULL);
  sqlite3_bind_text (stmt, 4, amsel_channel_get_source (channel), -1, NULL);

  rc = sqlite3_step (stmt);
  /* if (rc != SQLITE_DONE) */
  /*   g_error ("%s", sqlite3_errmsg (self->db)); */

  sqlite3_finalize (stmt);
  stmt = NULL;

  const GList *entries = amsel_channel_get_entries (channel);

  for (; entries; entries = g_list_next (entries))
    {
      AmselEntry *entry = AMSEL_ENTRY (entries->data);

      if (!stmt) {
        rc = sqlite3_prepare_v2 (self->db, "INSERT INTO entries VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", -1, &stmt, NULL);
        if (rc != SQLITE_OK)
          g_error ("%s", sqlite3_errmsg (self->db));
      } else {
        sqlite3_reset (stmt);
      }

      g_autofree gchar *date = g_date_time_format (amsel_entry_get_updated (entry), "%Y-%m-%dT%H:%M:%S%:z");

      sqlite3_bind_text (stmt, 1, amsel_entry_get_id (entry), -1, NULL);
      sqlite3_bind_text (stmt, 2, amsel_entry_get_title (entry), -1, NULL);
      sqlite3_bind_text (stmt, 3, amsel_entry_get_content (entry), -1, NULL);
      sqlite3_bind_text (stmt, 4, date, -1, NULL);
      sqlite3_bind_text (stmt, 5, amsel_entry_get_link (entry), -1, NULL);
      sqlite3_bind_text (stmt, 6, amsel_entry_get_preview_image (entry), -1, NULL);
      sqlite3_bind_int (stmt, 7, amsel_entry_get_read (entry));
      sqlite3_bind_text (stmt, 8, amsel_entry_get_author (entry), -1, NULL);
      sqlite3_bind_text (stmt, 9, amsel_channel_get_id (channel), -1, NULL);

      rc = sqlite3_step (stmt);
      if (rc != SQLITE_DONE) {
        if (rc != SQLITE_CONSTRAINT)
          g_error ("%s", sqlite3_errmsg (self->db));
        else {
          g_object_unref (entry);
        }
      } else {
        new_items = g_list_append (new_items, entry);
      }

    }

  sqlite3_exec (self->db, "COMMIT;", NULL, NULL, NULL);

  return new_items;
}

static GList *
amsel_sqlite_persistence_unwrap_channel (sqlite3_stmt *stmt)
{
  GList *channels = NULL;

  while (sqlite3_step (stmt) != SQLITE_DONE) {
    gint columns = sqlite3_column_count (stmt);

    // get plugin source first
    const guchar *plugin = sqlite3_column_text (stmt, 1);
    AmselChannel *channel = amsel_channel_new ((gchar *)plugin);

    for (gint i = 0; i < columns; i++)
      {
        const gchar *column_name = sqlite3_column_name (stmt, i);
        if (SQL_CMP ("id"))
          amsel_channel_set_id (channel, TEXT sqlite3_column_text (stmt, i));
        else if (SQL_CMP ("title"))
          amsel_channel_set_title (channel, TEXT sqlite3_column_text (stmt, i));
        else if (SQL_CMP ("source"))
          amsel_channel_set_source (channel, TEXT sqlite3_column_text (stmt, i));
      }
    channels = g_list_append (channels, channel);
  }

  return channels;
}

static GList *
amsel_sqlite_persistence_unwrap_entry (sqlite3_stmt *stmt)
{
  GList *entries = NULL;

  while (sqlite3_step (stmt) != SQLITE_DONE) {
    gint columns = sqlite3_column_count (stmt);

    AmselEntry *entry = amsel_entry_new ();

    for (gint i = 0; i < columns; i++)
      {
        const gchar *column_name = sqlite3_column_name (stmt, i);
        if (SQL_CMP ("id"))
          amsel_entry_set_id (entry, TEXT sqlite3_column_text (stmt, i));
        else if (SQL_CMP ("title"))
          amsel_entry_set_title (entry, TEXT sqlite3_column_text (stmt, i));
        else if (SQL_CMP ("content"))
          amsel_entry_set_content (entry, TEXT sqlite3_column_text (stmt, i));
        else if (SQL_CMP ("updated"))
          amsel_entry_set_updated (entry, TEXT sqlite3_column_text (stmt, i));
        else if (SQL_CMP ("link"))
          amsel_entry_set_link (entry, TEXT sqlite3_column_text (stmt, i));
        else if (SQL_CMP ("preview_image"))
          amsel_entry_set_preview_image (entry, TEXT sqlite3_column_text (stmt, i));
        else if (SQL_CMP ("read"))
          amsel_entry_set_read (entry, sqlite3_column_int (stmt, i));
        else if (SQL_CMP ("author"))
          amsel_entry_set_author (entry, TEXT sqlite3_column_text (stmt, i));
      }
    entries = g_list_append (entries, entry);
  }

  return entries;
}

static GList *
amsel_sqlite_persistence_get_channels (AmselFeedPersistence *persistence)
{
  g_return_val_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (persistence), NULL);

  GList *channels = NULL;
  gint rc;
  sqlite3_stmt *stmt;
  AmselSqlitePersistence *self = AMSEL_SQLITE_PERSISTENCE (persistence);

  rc = sqlite3_prepare_v2 (self->db, "SELECT id, plugin, title, source FROM channels", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  channels = amsel_sqlite_persistence_unwrap_channel (stmt);

  for (GList *cur = channels; cur; cur = g_list_next (cur))
    {
      AmselChannel *channel = AMSEL_CHANNEL (cur->data);
      GList *entries = amsel_feed_persistence_get_entries_for_channel (persistence, channel);
      for (; entries; entries = g_list_next (entries))
        {
          amsel_channel_add_entry (channel, entries->data);
        }
    }

  return channels;
}

static gint
amsel_sqlite_persistence_n_channels (AmselFeedPersistence *persistence)
{
  g_return_val_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (persistence), 0);

  gint rc;
  gint n_channels = 0;
  sqlite3_stmt *stmt;
  AmselSqlitePersistence *self = AMSEL_SQLITE_PERSISTENCE (persistence);

  rc = sqlite3_prepare_v2 (self->db, "SELECT COUNT(id) FROM channels", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  while ((rc = sqlite3_step (stmt)) == SQLITE_ROW) {
    n_channels = sqlite3_column_int (stmt, 0);
  }
  if (rc != SQLITE_DONE) {
    g_error ("rc: %d, %s", rc, sqlite3_errmsg (self->db));
  }

  return n_channels;
}

static GList *
amsel_sqlite_persistence_get_channels_for_plugin (AmselFeedPersistence *persistence,
                                                  const gchar          *plugin)
{
  g_return_val_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (persistence), NULL);

  GList *channels = NULL;
  gint rc;
  sqlite3_stmt *stmt;
  AmselSqlitePersistence *self = AMSEL_SQLITE_PERSISTENCE (persistence);

  rc = sqlite3_prepare_v2 (self->db, "SELECT id, plugin, title, source FROM channels WHERE plugin = ?", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  sqlite3_bind_text (stmt, 1, plugin, -1, NULL);

  channels = amsel_sqlite_persistence_unwrap_channel (stmt);

  return channels;
}

static GList *
amsel_sqlite_persistence_get_entries_for_channel (AmselFeedPersistence *persistence,
                                                  AmselChannel         *channel)
{
  g_return_val_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (persistence), NULL);

  GList *entries = NULL;
  const gchar *channel_id;
  sqlite3_stmt *stmt;
  AmselSqlitePersistence *self = AMSEL_SQLITE_PERSISTENCE (persistence);

  channel_id = amsel_channel_get_id (channel);
  sqlite3_prepare_v2 (self->db, "SELECT id, title, content, updated, link, preview_image, read, author, channel_id FROM entries WHERE channel_id=?", -1, &stmt, NULL);

  sqlite3_bind_text (stmt, 1, channel_id, -1, NULL);

  entries = amsel_sqlite_persistence_unwrap_entry (stmt);

  return entries;
}

static void
amsel_sqlite_persistence_mark_read (AmselFeedPersistence *persistence,
                                    AmselEntry           *entry,
                                    gboolean              read)
{
  g_return_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (persistence));

  gint rc;
  sqlite3_stmt *stmt;
  AmselSqlitePersistence *self = AMSEL_SQLITE_PERSISTENCE (persistence);

  rc = sqlite3_prepare_v2 (self->db, "UPDATE entries SET read=? WHERE id=?", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  sqlite3_bind_int (stmt, 1, read);
  sqlite3_bind_text (stmt, 2, amsel_entry_get_id (entry), -1, NULL);

  sqlite3_step (stmt);
}

static GList *
amsel_sqlite_persistence_delete_channel (AmselFeedPersistence *persistence,
                                         AmselChannel         *channel)
{
  gint rc;
  sqlite3_stmt *stmt;
  AmselSqlitePersistence *self = AMSEL_SQLITE_PERSISTENCE (persistence);

  rc = sqlite3_prepare_v2 (self->db, "DELETE FROM channels WHERE id=?", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  sqlite3_bind_text (stmt, 1, amsel_channel_get_id (channel), -1, NULL);

  sqlite3_step (stmt);
  return amsel_channel_get_entries (channel);
}

static void
amsel_sqlite_persistence_set_prop (AmselFeedPersistence *persistence,
                                   gchar                *name,
                                   gchar                *value)
{
  g_return_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (persistence));

  gint rc;
  sqlite3_stmt *stmt;
  AmselSqlitePersistence *self = AMSEL_SQLITE_PERSISTENCE (persistence);

  rc = sqlite3_prepare_v2 (self->db, "UPDATE properties SET value=? WHERE name=?", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  sqlite3_bind_text (stmt, 1, value, -1, NULL);
  sqlite3_bind_text (stmt, 2, name, -1, NULL);

  sqlite3_step (stmt);
}

static gchar *
amsel_sqlite_persistence_get_prop (AmselFeedPersistence *persistence,
                                   gchar                *name)
{
  g_return_val_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (persistence), NULL);

  gint rc;
  sqlite3_stmt *stmt;
  gchar *value = NULL;
  AmselSqlitePersistence *self = AMSEL_SQLITE_PERSISTENCE (persistence);

  rc = sqlite3_prepare_v2 (self->db, "SELECT value FROM properties WHERE name=?", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  sqlite3_bind_text (stmt, 1, name, -1, NULL);

  while ((rc = sqlite3_step (stmt)) == SQLITE_ROW) {
    value = g_strdup (TEXT sqlite3_column_text (stmt, 0));
  }
  if (rc != SQLITE_DONE) {
    g_error ("rc: %d, %s", rc, sqlite3_errmsg (self->db));
  }

  return value;
}

static void
amsel_insert_property (AmselSqlitePersistence *self,
                       gchar                  *name,
                       gchar                  *value)
{
  g_return_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (self));

  gint rc;
  sqlite3_stmt *stmt;

  rc = sqlite3_prepare_v2 (self->db, "INSERT INTO properties VALUES (?, ?)", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  sqlite3_bind_text (stmt, 1, name, -1, NULL);
  sqlite3_bind_text (stmt, 2, value, -1, NULL);

  sqlite3_step (stmt);
}

static void
amsel_sqlite_persistence_create_tables (AmselSqlitePersistence *self)
{
  g_return_if_fail (AMSEL_IS_SQLITE_PERSISTENCE (self));

  sqlite3_stmt *stmt;
  gchar *create_channel_table;
  gchar *create_entries_table;
  gchar *create_properties_table;
  gint rc;

  create_channel_table =
    "CREATE TABLE IF NOT EXISTS channels ("
    "   id     text PRIMARY KEY NOT NULL,"
    "   plugin text NOT NULL,"
    "   title  text,"
    "   source text NOT NULL)";

  sqlite3_prepare_v2 (self->db, create_channel_table, -1, &stmt, NULL);
  rc = sqlite3_step (stmt);
  if (rc != SQLITE_DONE)
    g_error ("%s", sqlite3_errmsg (self->db));
  sqlite3_finalize (stmt);

  create_entries_table =
    "CREATE TABLE IF NOT EXISTS entries ("
    "   id            text,"
    "   title         text,"
    "   content       text,"
    "   updated       text,"
    "   link          text,"
    "   preview_image text,"
    "   read          integer,"
    "   author        text,"
    "   channel_id    text,"
    "   PRIMARY KEY (id, channel_id),"
    "   CONSTRAINT fk_channel FOREIGN KEY(channel_id) REFERENCES channels(id) ON DELETE CASCADE)";

  sqlite3_prepare_v2 (self->db, create_entries_table, -1, &stmt, NULL);
  rc = sqlite3_step (stmt);
  if (rc != SQLITE_DONE)
    g_error ("%s", sqlite3_errmsg (self->db));
  sqlite3_finalize (stmt);

  create_properties_table =
    "CREATE TABLE IF NOT EXISTS properties ("
    "  name text PRIMARY KEY,"
    "  value text)";

  sqlite3_prepare_v2 (self->db, create_properties_table, -1, &stmt, NULL);
  rc = sqlite3_step (stmt);
  if (rc != SQLITE_DONE)
    g_error ("%s", sqlite3_errmsg (self->db));
  sqlite3_finalize (stmt);

  amsel_insert_property (self, "refresh-interval", "10");
}

AmselSqlitePersistence *
amsel_sqlite_persistence_new (void)
{
  return g_object_new (AMSEL_TYPE_SQLITE_PERSISTENCE, NULL);
}

static void
amsel_sqlite_persistence_finalize (GObject *object)
{
  AmselSqlitePersistence *self = (AmselSqlitePersistence *)object;

  sqlite3_close (self->db);

  G_OBJECT_CLASS (amsel_sqlite_persistence_parent_class)->finalize (object);
}


static void
amsel_sqlite_persistence_class_init (AmselSqlitePersistenceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = amsel_sqlite_persistence_finalize;
}

static void
amsel_sqlite_persistence_init (AmselSqlitePersistence *self)
{
  g_autofree gchar *dbfile = g_build_filename (g_get_user_data_dir (), "amsel.db", NULL);
  g_debug ("Database Location: %s", dbfile);
  sqlite3_open (dbfile, &self->db);

  amsel_sqlite_persistence_create_tables (self);

  sqlite3_exec (self->db, "PRAGMA foreign_keys = ON", NULL, NULL, NULL);
}

static void
persistence_iface_init (AmselFeedPersistenceInterface *iface)
{
  iface->store = amsel_sqlite_persistence_store;
  iface->get_channels = amsel_sqlite_persistence_get_channels;
  iface->n_channels = amsel_sqlite_persistence_n_channels;
  iface->get_channels_for_plugin = amsel_sqlite_persistence_get_channels_for_plugin;
  iface->get_entries_for_channel = amsel_sqlite_persistence_get_entries_for_channel;
  iface->mark_read = amsel_sqlite_persistence_mark_read;
  iface->delete_channel = amsel_sqlite_persistence_delete_channel;
  iface->set_prop = amsel_sqlite_persistence_set_prop;
  iface->get_prop = amsel_sqlite_persistence_get_prop;
}
