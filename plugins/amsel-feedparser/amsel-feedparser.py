#!/usr/bin/env python2

import logging

from gi.repository import GLib
from gi.repository import GObject
from gi.repository import Amsel
from gi.repository import GdkPixbuf
import feedparser
import pprint
import re
from time import mktime
from datetime import datetime
from html.parser import HTMLParser

logger = logging.getLogger(__name__)

class Feedparser(GObject.Object, Amsel.FeedProvider):

        def do_load(self):
            pass


        def do_get_icon(self):
            return GdkPixbuf.Pixbuf.new_from_file ("/app/lib/amsel/plugins/icon.png")


        def do_retrieve(self, url):
            ret = feedparser.parse (url)

            if ret['bozo'] == 1:
                return
            channel = self.get_channel (ret, url)

            entries = iter (ret.entries)
            for entry in entries:
                # pprint.pprint (entry)
                amselentry = Amsel.Entry()
                amselentry.set_id (entry.id)
                amselentry.set_title (entry.title)
                if 'content' in entry:
                    content = entry.content[0].value
                    amselentry.set_content (content)
                    preview_image = self.get_first_article_image(content)
                    if preview_image is not None:
                        amselentry.set_preview_image (preview_image)
                else:
                    amselentry.set_content (entry.summary)

                if 'updated' in entry:
                    amselentry.set_updated (entry.updated)
                elif 'published' in entry:
                    dt = entry.published_parsed
                    gdt = GLib.DateTime.new(GLib.TimeZone.new(None), dt.tm_year, dt.tm_mon, dt.tm_mday, dt.tm_hour, dt.tm_min, dt.tm_sec)
                    amselentry.set_updated_datetime (gdt)

                links = iter(entry.links)
                for link in links:
                    if link.type == 'text/html' and link.rel == 'alternate':
                        amselentry.set_link (link.href)

                if 'author' in entry:
                    amselentry.set_author (entry.author)

                channel.add_entry (amselentry)

            return channel

        def get_channel (self, doc, url):
            instance = Amsel.Channel.new(self.do_get_name())

            pprint.pprint (doc.feed)
            if 'title' in doc.feed:
                instance.set_title (doc.feed.title)
            instance.set_icon (self.get_icon (doc))

            if 'links' in doc.feed:
                links = iter(doc.feed.links)
                for link in links:
                    if ((link.type == 'application/atom+xml' and link.rel == 'self') or
                        (link.type == 'application/rss+xml' and link.rel == 'self')):
                        instance.set_source (link.href)
            else:
                instance.set_source (url)

            if 'id' in doc.feed:
                instance.set_id (doc.feed.id)
            else:
                instance.set_id (instance.get_source ())
            return instance


        def get_icon(self, doc):
            if 'icon' in doc.feed:
                return doc.feed.icon

            # propably there is no icon therefore returning favicon
            if 'link' in doc.feed:
                return doc.feed.link + "/favicon.ico"
            return ""


        def get_first_article_image(self, content):
            parser = MyHTMLParser()
            parser.feed(content)
            img = parser.get_image()
            return img


        def do_get_name(self):
            return "local"

# create a subclass and override the handler methods
class MyHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.image = None

    def handle_starttag(self, tag, attrs):
        if tag == 'img':
            for attr in attrs:
                if attr[0] == 'src':
                    if self.image is None:
                        self.image = attr[1]

    def get_image(self):
        image = self.image
        self.image = None
        return image