/* main.c
 *
 * Copyright 2018 Guenther Wutz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <gio/gio.h>

#include "amsel-config.h"
#include "amsel-application.h"
#include "amsel-debug.h"

static const gchar *
ide_log_level_str_with_color (GLogLevelFlags log_level)
{
  switch (((gulong)log_level & G_LOG_LEVEL_MASK))
    {
    case G_LOG_LEVEL_ERROR:      return "   \033[1;31mERROR\033[0m";
    case G_LOG_LEVEL_CRITICAL:   return "\033[1;35mCRITICAL\033[0m";
    case G_LOG_LEVEL_WARNING:    return " \033[1;33mWARNING\033[0m";
    case G_LOG_LEVEL_MESSAGE:    return " \033[1;32mMESSAGE\033[0m";
    case G_LOG_LEVEL_INFO:       return "    \033[1;32mINFO\033[0m";
    case G_LOG_LEVEL_DEBUG:      return "   \033[1;32mDEBUG\033[0m";
    case AMSEL_LOG_LEVEL_TRACE:  return "   \033[1;36mTRACE\033[0m";

    default:
      return " UNKNOWN";
    }
}

void
amsel_log_handler (const gchar    *log_domain,
                   GLogLevelFlags  log_level,
                   const gchar    *message,
                   gpointer        user_data)
{
  g_print ("\t%s [%s] %s\n", ide_log_level_str_with_color (log_level), log_domain, message);
}

int
main (int   argc,
      char *argv[])
{
	g_autoptr(AmselApplication) app = NULL;
	int ret;

	/* Set up gettext translations */
	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

  g_log_set_default_handler (amsel_log_handler, NULL);
	/*
	 * Create a new GtkApplication. The application manages our main loop,
	 * application windows, integration with the window manager/compositor, and
	 * desktop features such as file opening and single-instance applications.
	 */
  app = amsel_application_new ("org.gnome.Amsel", G_APPLICATION_FLAGS_NONE);

	/*
	 * We connect to the activate signal to create a window when the application
	 * has been lauched. Additionally, this signal notifies us when the user
	 * tries to launch a "second instance" of the application. When they try
	 * to do that, we'll just present any existing window.
	 *
	 * Because we can't pass a pointer to any function type, we have to cast
	 * our "on_activate" function to a GCallback.
	 */
	/* g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL); */

	/*
	 * Run the application. This function will block until the applicaiton
	 * exits. Upon return, we have our exit code to return to the shell. (This
	 * is the code you see when you do `echo $?` after running a command in a
	 * terminal.
	 *
	 * Since GtkApplication inherits from GApplication, we use the parent class
	 * method "run". But we need to cast, which is what the "G_APPLICATION()"
	 * macro does.
	 */
	ret = g_application_run (G_APPLICATION (app), argc, argv);
	return ret;
}
