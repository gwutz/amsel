/* amsel-feed-persistence.h
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include <glib-object.h>
#include <model/amsel-channel.h>

G_BEGIN_DECLS

#define AMSEL_TYPE_FEED_PERSISTENCE (amsel_feed_persistence_get_type ())

G_DECLARE_INTERFACE (AmselFeedPersistence, amsel_feed_persistence, AMSEL, FEED_PERSISTENCE, GObject)

struct _AmselFeedPersistenceInterface
{
  GTypeInterface parent;

  GList *(*store) (AmselFeedPersistence *self,
                   AmselChannel         *channel);
  GList *(*get_channels) (AmselFeedPersistence *self);
  gint   (*n_channels) (AmselFeedPersistence *self);
  GList *(*get_channels_for_plugin) (AmselFeedPersistence *self,
                                     const gchar          *plugin);
  GList *(*get_entries_for_channel) (AmselFeedPersistence *self,
                                     AmselChannel         *channel);
  void   (*mark_read)               (AmselFeedPersistence *self,
                                     AmselEntry           *entry,
                                     gboolean              read);
  GList *(*delete_channel)          (AmselFeedPersistence *self,
                                     AmselChannel         *channel);
  void   (*set_prop)                (AmselFeedPersistence *self,
                                     gchar                *name,
                                     gchar                *value);
  gchar *(*get_prop)                (AmselFeedPersistence *self,
                                     gchar                *name);
};

GList  *amsel_feed_persistence_store                    (AmselFeedPersistence *self,
                                                         AmselChannel         *channel);
GList  *amsel_feed_persistence_get_channels             (AmselFeedPersistence *self);
gint    amsel_feed_persistence_n_channels               (AmselFeedPersistence *self);
GList  *amsel_feed_persistence_get_channels_for_plugin  (AmselFeedPersistence *self,
                                                         const gchar          *plugin);
GList  *amsel_feed_persistence_get_entries_for_channel  (AmselFeedPersistence *self,
                                                         AmselChannel         *channel);
void    amsel_feed_persistence_mark_read                (AmselFeedPersistence *self,
                                                         AmselEntry           *entry,
                                                         gboolean              read);
GList  *amsel_feed_persistence_delete_channel           (AmselFeedPersistence *self,
                                                         AmselChannel         *channel);
void    amsel_feed_persistence_set_prop                 (AmselFeedPersistence *self,
                                                         gchar                *name,
                                                         gchar                *value);
gchar  *amsel_feed_persistence_get_prop                 (AmselFeedPersistence *self,
                                                         gchar                *name);
G_END_DECLS
