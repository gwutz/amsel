/* amsel-engine.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "amsel-engine"

#include "amsel-engine.h"

#include <libpeas/peas.h>
#include <girepository.h>
#include <amsel-config.h>
#include <amsel-feed-persistence.h>

#include <amsel-debug.h>

/**
 * SECTION:amsel-engine
 * @title: AmselEngine
 *
 * This is the main gateway to operate within libamsel. The engine holds all
 * necessary plugins for providing feeds and persist them.
 *
 * ![Sequence Diagram AmselEngine](sequence-engine.png)
 */

struct _AmselEngine
{
  GObject parent_instance;

  AmselFeedProviderManager *provider_manager;
  AmselFeedPersistence *persistence;
};

G_DEFINE_TYPE (AmselEngine, amsel_engine, G_TYPE_OBJECT)

enum {
  CHANNEL_FETCHED,
  ITEM_FETCHED,
  NEW_ITEMS,
  REFRESH,
  N_SIGNALS,
};

static gint signals[N_SIGNALS];

/**
 * amsel_engine_new:
 *
 * Creates the #AmselEngine
 *
 * Returns: the #AmselEngine
 */
AmselEngine *
amsel_engine_new (void)
{
  return g_object_new (AMSEL_TYPE_ENGINE,
                       NULL);
}

static void
amsel_engine_finalize (GObject *object)
{
  AmselEngine *self = (AmselEngine *)object;

  g_clear_object (&self->provider_manager);
  g_clear_object (&self->persistence);

  G_OBJECT_CLASS (amsel_engine_parent_class)->finalize (object);
}

static void
amsel_engine_class_init (AmselEngineClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = amsel_engine_finalize;

  /**
   * AmselEngine::item-fetched:
   * @engine: the #AmselEngine which fetched the new #AmselEntry objects
   * @entry: a new #AmselEntry
   *
   * The :item-fetched signal is emited for every new #AmselEntry object
   */
  signals [ITEM_FETCHED] =
    g_signal_new ("item-fetched",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  1,
                  AMSEL_TYPE_ENTRY
                  );

  /**
   * AmselEngine::channel-fetched:
   * @engine: the #AmselEngine which fetched the new #AmselChannel object
   * @channel: a new #AmselChannel
   *
   * The :channel-fetched signal is emited for every new #AmselChannel object.
   * Note: every subscriber gets a reference to the channel object. Cleanup if
   * you don't need it
   */
  signals [CHANNEL_FETCHED] =
    g_signal_new ("channel-fetched",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  1,
                  AMSEL_TYPE_CHANNEL
                  );

  /**
   * AmselEngine::new-items:
   * @engine: the #AmselEngine
   * @items: how many items are new
   *
   * The :new-items signal is emited when new items are saved in the backend
   */
  signals [NEW_ITEMS] =
    g_signal_new ("new-items",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  1,
                  G_TYPE_INT
                  );

  /**
   * AmselEngine::refresh:
   * @engine: the #AmselEngine
   * @n_channels: how many channels get fetched
   *
   * The :refresh signal is emited when the engine refreshes all channels
   */
  signals [REFRESH] =
    g_signal_new ("refresh",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  1,
                  G_TYPE_INT
                  );
}

static void
amsel_engine_discover_plugins (AmselEngine *self)
{
  const GList *list;
  PeasEngine *engine = peas_engine_new ();
  AmselFeedProviderManager *provider_manager;

  provider_manager = amsel_engine_get_amsel_feed_provider_manager (self);

  g_irepository_prepend_search_path (PACKAGE_LIBDIR"/amsel/girepository-1.0");

  peas_engine_prepend_search_path (engine, PACKAGE_LIBDIR"/amsel/plugins", PACKAGE_DATADIR"/amsel/plugins");

  g_irepository_require (NULL, "Amsel", "1.0", 0, NULL);
  peas_engine_enable_loader (engine, "python3");

  list = peas_engine_get_plugin_list (engine);

  for (; list; list = g_list_next (list))
    {
      PeasPluginInfo *plugin_info = list->data;

      g_debug ("Discovered Plugin \"%s\"",
               peas_plugin_info_get_module_name (plugin_info));

      if (peas_engine_load_plugin (engine, plugin_info))
        {
          if (peas_engine_provides_extension (engine, plugin_info, AMSEL_TYPE_FEED_PROVIDER))
            {
              PeasExtension *plugin = peas_engine_create_extension (engine, plugin_info, AMSEL_TYPE_FEED_PROVIDER, NULL);
              AmselFeedProvider *provider = AMSEL_FEED_PROVIDER (plugin);

              amsel_feed_provider_manager_add_provider (provider_manager, amsel_feed_provider_get_name (provider), provider);
            }

          if (peas_engine_provides_extension (engine, plugin_info, AMSEL_TYPE_FEED_PERSISTENCE))
            {
              PeasExtension *plugin = peas_engine_create_extension (engine, plugin_info, AMSEL_TYPE_FEED_PERSISTENCE, NULL);
              self->persistence = AMSEL_FEED_PERSISTENCE (plugin);
            }
        }
    }
}

static void
amsel_engine_init (AmselEngine *self)
{
  self->provider_manager = amsel_feed_provider_manager_new ();

  amsel_engine_discover_plugins (self);
}

static void
amsel_engine_fetch_finished (GObject      *source_object,
                             GAsyncResult *res,
                             gpointer      user_data)
{
  AmselEngine *self = AMSEL_ENGINE (user_data);
  AmselFeedProvider *provider = AMSEL_FEED_PROVIDER (source_object);
  g_autoptr(AmselChannel) channel;

  AMSEL_PROBE;

  channel = amsel_feed_provider_retrieve_finish (provider, res, NULL);
  g_signal_emit (self, signals[CHANNEL_FETCHED], 0, g_object_ref (channel));

  GList *new_items = amsel_feed_persistence_store (self->persistence, channel);

  gint new_items_count = g_list_length (new_items);

  if (new_items_count > 0)
    g_signal_emit (self, signals[NEW_ITEMS], 0, new_items_count);

  for(; new_items; new_items = g_list_next (new_items))
    {
      AmselEntry *entry = AMSEL_ENTRY (new_items->data);
      g_signal_emit (self, signals[ITEM_FETCHED], 0, entry);
    }
}

void
amsel_engine_fetch (AmselEngine *self,
                    const gchar *url,
                    const gchar *plugin)
{
  g_return_if_fail (AMSEL_IS_ENGINE (self));

  g_autoptr (AmselFeedProvider) provider;
  AmselFeedProviderManager *provider_manager;

  AMSEL_PROBE;

  provider_manager = amsel_engine_get_amsel_feed_provider_manager (self);
  provider = amsel_feed_provider_manager_get_provider (provider_manager, plugin);
  g_debug ("Retrieved Provider: %s", plugin);

  amsel_feed_provider_retrieve_async (provider, url, NULL, amsel_engine_fetch_finished, self);
}

/**
 * amsel_engine_get_amsel_feed_provider_manager:
 * @self: a #AmselEngine
 *
 * Returns the #AmselFeedProviderManager
 *
 * Returns: (transfer none): the #AmselFeedProviderManager
 */
AmselFeedProviderManager *
amsel_engine_get_amsel_feed_provider_manager (AmselEngine *self)
{
  g_return_val_if_fail (AMSEL_IS_ENGINE (self), NULL);

  return self->provider_manager;
}

/**
 * amsel_engine_get_all_entries:
 * @self: a #AmselEngine
 *
 * Gather all entries from the persistence layer and return them in a list. The
 * items in the list are not sorted.
 * Clear the list after usage and free the items.
 *
 * Returns: (element-type AmselEntry) (transfer full): get all saved #AmselEntry objects
 */
GList *
amsel_engine_get_all_entries (AmselEngine *self)
{
  g_return_val_if_fail (AMSEL_IS_ENGINE (self), NULL);
  GList *all_entries = NULL;

  GList *channels = amsel_feed_persistence_get_channels (self->persistence);
  for (; channels; channels = g_list_next (channels))
    {
      GList *entries = amsel_feed_persistence_get_entries_for_channel (self->persistence, channels->data);
      all_entries = g_list_concat (all_entries, entries);
    }
  g_list_free_full (channels, g_object_unref);

  return all_entries;
}

void
amsel_engine_refresh_all (AmselEngine *self)
{
  g_return_if_fail (AMSEL_IS_ENGINE (self));

  GList *channels;
  gint n_channels;

  channels = amsel_feed_persistence_get_channels (self->persistence);
  n_channels = g_list_length (channels);
  g_signal_emit (self, signals[REFRESH], 0, n_channels);

  for (; channels; channels = g_list_next (channels))
    {
      const gchar *plugin;
      const gchar *url;
      AmselChannel *channel = AMSEL_CHANNEL (channels->data);

      url = amsel_channel_get_source (channel);
      plugin = amsel_channel_get_plugin (channel);

      amsel_engine_fetch (self, url, plugin);
    }

  g_list_free_full (channels, g_object_unref);
}

/**
 * amsel_engine_n_channels:
 * @self: a #AmselEngine
 *
 * This functions determines how much channels are subscribed.
 *
 * Returns: the subscribed channel length
 */
gint
amsel_engine_n_channels (AmselEngine *self)
{
  g_return_val_if_fail (AMSEL_IS_ENGINE (self), 0);

  return amsel_feed_persistence_n_channels (self->persistence);
}

/**
 * amsel_engine_get_all_channels:
 * @self: a #AmselEngine
 *
 * get all #AmselChannel objects saved in persistence layer
 *
 * Returns: (transfer full) (element-type AmselChannel): the list with all
 *                                                       #AmselChannel s
 */
GList *
amsel_engine_get_all_channels (AmselEngine *self)
{
  g_return_val_if_fail (AMSEL_IS_ENGINE (self), NULL);

  return amsel_feed_persistence_get_channels (self->persistence);
}

void
amsel_engine_mark_read (AmselEngine *self,
                        AmselEntry  *entry,
                        gboolean     read)
{
  g_return_if_fail (AMSEL_IS_ENGINE (self));
  g_return_if_fail (AMSEL_IS_ENTRY (entry));

  amsel_feed_persistence_mark_read (self->persistence, entry, read);
}

GList *
amsel_engine_delete_channel (AmselEngine  *self,
                             AmselChannel *channel)
{
  g_return_val_if_fail (AMSEL_IS_ENGINE (self), NULL);
  g_return_val_if_fail (AMSEL_IS_CHANNEL (channel), NULL);

  return amsel_feed_persistence_delete_channel (self->persistence, channel);
}

void
amsel_engine_set_prop (AmselEngine *self,
                       gchar       *name,
                       gchar       *value)
{
  g_return_if_fail (AMSEL_IS_ENGINE (self));

  amsel_feed_persistence_set_prop (self->persistence, name, value);
}

gchar *
amsel_engine_get_prop (AmselEngine *self,
                       gchar       *name)
{
  g_return_val_if_fail (AMSEL_IS_ENGINE (self), NULL);

  return amsel_feed_persistence_get_prop (self->persistence, name);
}
