#pragma once

#include <gtk/gtk.h>
#include <amsel-feed-provider.h>
#include "amsel-engine.h"
#include "amsel-store.h"

G_BEGIN_DECLS

#define AMSEL_TYPE_APPLICATION (amsel_application_get_type())

G_DECLARE_FINAL_TYPE (AmselApplication, amsel_application, AMSEL, APPLICATION, GtkApplication)

AmselApplication  *amsel_application_new (gchar *application_id, GApplicationFlags flags);
GtkApplicationWindow *amsel_application_get_window (AmselApplication *self);
AmselEngine       *amsel_application_get_engine (AmselApplication *self);
void               amsel_application_setup_refresh (AmselApplication *self);
AmselStore        *amsel_application_get_store (AmselApplication *self);

G_END_DECLS
