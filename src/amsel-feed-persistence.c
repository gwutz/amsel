/* amsel-feed-persistence.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include "amsel-feed-persistence.h"

G_DEFINE_INTERFACE (AmselFeedPersistence, amsel_feed_persistence, G_TYPE_OBJECT)

/**
 * AmselFeedPersistenceInterface:
 * @parent: parent #GTypeInterface
 * @store: the vfunc to store the #AmselChannel and all his #AmselEntry items
 * @get_channels: get all #AmselChannel stored
 * @n_channels: get the number of #AmselChannel stored
 * @get_channels_for_plugin: get all #AmselChannel from the specific plugin
 * @get_entries_for_channel: get all #AmselEntry for the specific #AmselChannel
 * @mark_read: mark the #AmselEntry as read or unread
 * @delete_channel: delete the #AmselChannel from the database
 */

static void
amsel_feed_persistence_default_init (AmselFeedPersistenceInterface *iface)
{
  iface->store = amsel_feed_persistence_store;
  iface->get_channels = amsel_feed_persistence_get_channels;
  iface->get_channels_for_plugin = amsel_feed_persistence_get_channels_for_plugin;
  iface->get_entries_for_channel = amsel_feed_persistence_get_entries_for_channel;
  iface->n_channels = amsel_feed_persistence_n_channels;
  iface->mark_read = amsel_feed_persistence_mark_read;
  iface->delete_channel = amsel_feed_persistence_delete_channel;
  iface->set_prop = amsel_feed_persistence_set_prop;
  iface->get_prop = amsel_feed_persistence_get_prop;
}

/**
 * amsel_feed_persistence_store:
 * @self: a #AmselFeedPersistence
 * @channel: a #AmselChannel
 *
 * Stores the #AmselChannel and the corresponding #AmselEntry children.
 *
 * Returns: (element-type AmselEntry) (transfer container): get all stored #AmselChannels
 */
GList *
amsel_feed_persistence_store (AmselFeedPersistence *self,
                              AmselChannel         *channel)
{
  return AMSEL_FEED_PERSISTENCE_GET_IFACE (self)->store (self, channel);
}


/**
 * amsel_feed_persistence_get_channels:
 * @self: a #AmselFeedPersistence
 *
 * Returns: (element-type AmselChannel) (transfer full): get all stored #AmselChannels
 */
GList *
amsel_feed_persistence_get_channels (AmselFeedPersistence *self)
{
  return AMSEL_FEED_PERSISTENCE_GET_IFACE (self)->get_channels (self);
}

/**
 * amsel_feed_persistence_get_channels_for_plugin:
 * @self: a #AmselFeedPersistence
 * @plugin: a #AmselFeedProvider identifier
 *
 * Returns: (element-type AmselChannel) (transfer full): get all stored #AmselChannels
 */
GList *
amsel_feed_persistence_get_channels_for_plugin (AmselFeedPersistence *self,
                                                const gchar          *plugin)
{
  return AMSEL_FEED_PERSISTENCE_GET_IFACE (self)->get_channels_for_plugin (self, plugin);
}

/**
 * amsel_feed_persistence_get_entries_for_channel:
 * @self: a #AmselFeedPersistence
 * @channel: a #AmselChannel
 *
 * Returns: (element-type AmselEntry) (transfer full): get all stored #AmselEntry for #AmselChannel
 */
GList *
amsel_feed_persistence_get_entries_for_channel (AmselFeedPersistence *self,
                                                AmselChannel         *channel)
{
  return AMSEL_FEED_PERSISTENCE_GET_IFACE (self)->get_entries_for_channel (self, channel);
}

/**
 * amsel_feed_persistence_n_channels:
 *
 * Returns the number of channels which are stored.
 *
 * Returns: the number of channels
 */
gint
amsel_feed_persistence_n_channels (AmselFeedPersistence *self)
{
  return AMSEL_FEED_PERSISTENCE_GET_IFACE (self)->n_channels (self);
}

/**
 * amsel_feed_persistence_mark_read:
 * @self: a #AmselFeedPersistence
 * @entry: a #AmselEntry
 * @read: the boolean read status of #AmselEntry
 *
 * Marks the read/unread status of the #AmselEntry
 */
void
amsel_feed_persistence_mark_read (AmselFeedPersistence *self,
                                  AmselEntry           *entry,
                                  gboolean              read)
{
  AMSEL_FEED_PERSISTENCE_GET_IFACE (self)->mark_read (self, entry, read);
}

/**
 * amsel_feed_persistence_delete_channel:
 * @self: a #AmselFeedPersistence
 * @channel: a #AmselChannel
 *
 * Returns: (transfer full) (element-type AmselEntry): the deleted entry elements
 */
GList *
amsel_feed_persistence_delete_channel (AmselFeedPersistence *self,
                                       AmselChannel         *channel)
{
  return AMSEL_FEED_PERSISTENCE_GET_IFACE (self)->delete_channel (self, channel);
}

/**
 * amsel_feed_persistence_set_prop:
 * @self: a #AmselFeedPersistence
 * @name: the name of the property
 * @value: the value of the property
 *
 * sets a application property into the database
 */
void
amsel_feed_persistence_set_prop (AmselFeedPersistence *self,
                                 gchar                *name,
                                 gchar                *value)
{
  AMSEL_FEED_PERSISTENCE_GET_IFACE (self)->set_prop (self, name, value);
}

/**
 * amsel_feed_persistence_get_prop:
 * @self: a #AmselFeedPersistence
 * @name: the name of the property
 *
 * retrieves the property from the database
 *
 * Returns: the value of the property
 */
gchar *
amsel_feed_persistence_get_prop (AmselFeedPersistence *self,
                                 gchar                *name)
{
  return AMSEL_FEED_PERSISTENCE_GET_IFACE (self)->get_prop (self, name);
}
