/* amsel-date.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "amsel-date.h"

struct _AmselDate
{
  GObject parent_instance;
  GDate *date;
};

G_DEFINE_TYPE (AmselDate, amsel_date, G_TYPE_OBJECT)


AmselDate *
amsel_date_new (GDate *date)
{
  AmselDate *self = g_object_new (AMSEL_TYPE_DATE, NULL);
  self->date = date;
  return self;
}

static void
amsel_date_finalize (GObject *object)
{
  AmselDate *self = (AmselDate *)object;

  g_date_free (self->date);

  G_OBJECT_CLASS (amsel_date_parent_class)->finalize (object);
}

static void
amsel_date_class_init (AmselDateClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = amsel_date_finalize;
}

static void
amsel_date_init (AmselDate *self)
{
}

GDate *
amsel_date_get_date (AmselDate *self)
{
  return self->date;
}
