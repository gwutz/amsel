/* amsel-feed-popover.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "amsel-feed-popover"
#include "amsel-feed-popover.h"
#include "amsel-application.h"
#include <libsoup/soup.h>

struct _AmselFeedPopover
{
  GtkPopover parent_instance;

  GtkComboBoxText *provider_list;
  GtkEntry *url_entry;
  GtkButton *add_button;
};

G_DEFINE_TYPE (AmselFeedPopover, amsel_feed_popover, GTK_TYPE_POPOVER)

AmselFeedPopover *
amsel_feed_popover_new (void)
{
  return g_object_new (AMSEL_TYPE_FEED_POPOVER,
                       NULL);
}

static void
amsel_feed_popover_constructed (GObject *object)
{
  AmselFeedPopover *self = (AmselFeedPopover *)object;
  AmselApplication *app = AMSEL_APPLICATION (g_application_get_default ());
  AmselEngine *engine = amsel_application_get_engine (app);

  AmselFeedProviderManager *provider_manager = amsel_engine_get_amsel_feed_provider_manager (engine);

  GList *l = amsel_feed_provider_manager_get_provider_list (provider_manager);

  for (; l; l = g_list_next (l))
    {
      gchar *provideritem = l->data;
      AmselFeedProvider *provider = amsel_feed_provider_manager_get_provider (provider_manager, provideritem);
      gtk_combo_box_text_append_text (self->provider_list, amsel_feed_provider_get_name (provider));
    }

  gtk_combo_box_set_active (GTK_COMBO_BOX (self->provider_list), 0);
  g_list_free (l);

  G_OBJECT_CLASS (amsel_feed_popover_parent_class)->constructed (object);
}

static void
amsel_feed_popover_add_clicked (GtkButton *btn,
                                gpointer   user_data)
{
  AmselFeedPopover *self = AMSEL_FEED_POPOVER (user_data);

  gchar *selected_provider = gtk_combo_box_text_get_active_text (self->provider_list);

  g_debug ("Selected Provider: %s", selected_provider);

  const gchar *url = gtk_entry_get_text (self->url_entry);
  AmselApplication *app = AMSEL_APPLICATION (g_application_get_default ());
  AmselEngine *engine = amsel_application_get_engine (app);

  amsel_engine_fetch (engine, url, selected_provider);
  gtk_entry_set_text (self->url_entry, "");
}

static void
amsel_feed_popover_entry_activated (GtkEntry *entry,
                                    gpointer  user_data)
{
  amsel_feed_popover_add_clicked (NULL, user_data);
}

static void
amsel_feed_popover_url_changed (GtkEditable *editable,
                                gpointer     user_data)
{
  const gchar *url;
  AmselFeedPopover *self = AMSEL_FEED_POPOVER (user_data);

  url = gtk_entry_get_text (self->url_entry);

  g_autoptr(SoupURI) uri = soup_uri_new (url);
  if (!uri)
    {
      gtk_widget_set_sensitive (GTK_WIDGET (self->add_button), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive (GTK_WIDGET (self->add_button), TRUE);
    }

}

static void
amsel_feed_popover_finalize (GObject *object)
{
  /* AmselFeedPopover *self = (AmselFeedPopover *)object; */

  G_OBJECT_CLASS (amsel_feed_popover_parent_class)->finalize (object);
}

static void
amsel_feed_popover_class_init (AmselFeedPopoverClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = amsel_feed_popover_finalize;
  object_class->constructed = amsel_feed_popover_constructed;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Amsel/feed-popover.ui");
  gtk_widget_class_bind_template_child (widget_class, AmselFeedPopover, provider_list);
  gtk_widget_class_bind_template_child (widget_class, AmselFeedPopover, url_entry);
  gtk_widget_class_bind_template_child (widget_class, AmselFeedPopover, add_button);
  gtk_widget_class_bind_template_callback (widget_class, amsel_feed_popover_add_clicked);
  gtk_widget_class_bind_template_callback (widget_class, amsel_feed_popover_entry_activated);
  gtk_widget_class_bind_template_callback (widget_class, amsel_feed_popover_url_changed);
}

static void
amsel_feed_popover_init (AmselFeedPopover *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
