/* amsel-preferences.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include "amsel-preferences.h"
#include "amsel-application.h"
#include "model/amsel-channel.h"
#include "amsel-debug.h"
#include "opml/amsel-opml.h"

struct _AmselPreferences
{
  GtkDialog parent_instance;

  GtkListBox *channels;
  GListStore *channel_store;
  GtkSpinButton *refresh_spin_btn;
  AmselOpml *opmlparser;
};

G_DEFINE_TYPE (AmselPreferences, amsel_preferences, GTK_TYPE_DIALOG)

static void
amsel_preferences_delete_channel (GtkButton *btn,
                                  gpointer   user_data)
{
  AmselEngine *engine;
  AmselChannel *channel;
  AmselPreferences *self = AMSEL_PREFERENCES (user_data);
  GApplication *app = g_application_get_default ();

  engine = amsel_application_get_engine (AMSEL_APPLICATION (app));

  // find ListBoxRow
  GtkWidget *cur = GTK_WIDGET (btn);
  while (!GTK_IS_LIST_BOX_ROW (cur))
    cur = gtk_widget_get_parent (cur);

  gint index = gtk_list_box_row_get_index (GTK_LIST_BOX_ROW (cur));

  channel = g_list_model_get_item (G_LIST_MODEL (self->channel_store), index);
  GList *deleted_entries = amsel_engine_delete_channel (engine, channel);
  g_list_store_remove (self->channel_store, index);

  AmselStore *store = amsel_application_get_store (AMSEL_APPLICATION (app));
  AMSEL_PROBE;
  for (; deleted_entries; deleted_entries = g_list_next (deleted_entries))
    {
      AMSEL_PROBE;
      AmselEntry *entry = AMSEL_ENTRY (deleted_entries->data);
      amsel_store_remove (store, entry);
    }

  /* AmselDataManager *data_manager = amsel_application_get_data_manager (AMSEL_APPLICATION (app)); */
  /* GListStore *store = amsel_data_manager_get_store (data_manager); */
  /* g_list_store_remove_all (store); */
  /* GList *entries = amsel_engine_get_all_entries (engine); */

  /* for (; entries; entries = g_list_next (entries)) */
  /*   { */
  /*     amsel_data_manager_add_entry (data_manager, entries->data); */
  /*   } */
}

GtkWidget *
amsel_preferences_create_channel_row (gpointer item,
                                      gpointer user_data)
{
  AmselPreferences *self = AMSEL_PREFERENCES (user_data);
  AmselChannel *channel;

  channel = AMSEL_CHANNEL (item);

  GtkWidget *main_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_widget_set_margin_top (main_box, 5);
  gtk_widget_set_margin_bottom (main_box, 5);

  // const gchar *icon = amsel_channel_get_icon (channel);
  // TODO: fetch icon and show it

  GtkWidget *lbl = gtk_label_new (amsel_channel_get_title (channel));
  gtk_label_set_xalign (GTK_LABEL (lbl), 0.f);

  GtkWidget *trashicon = gtk_image_new_from_icon_name ("user-trash-symbolic", GTK_ICON_SIZE_BUTTON);
  GtkWidget *trashbtn = gtk_button_new ();
  gtk_button_set_image (GTK_BUTTON (trashbtn), trashicon);
  gtk_button_set_relief (GTK_BUTTON (trashbtn), GTK_RELIEF_NONE);

  g_signal_connect (trashbtn, "clicked", G_CALLBACK (amsel_preferences_delete_channel), self);

  gtk_box_pack_start (GTK_BOX (main_box), lbl, TRUE, TRUE, 5);
  gtk_box_pack_start (GTK_BOX (main_box), trashbtn, FALSE, FALSE, 5);

  gtk_widget_show_all (main_box);

  return main_box;
}

static void
amsel_preferences_close (GtkButton *btn,
                         gpointer   user_data)
{
  AmselPreferences *self = AMSEL_PREFERENCES (user_data);

  gtk_dialog_response (GTK_DIALOG (self), GTK_RESPONSE_CLOSE);
  gtk_widget_destroy (GTK_WIDGET (self));
}

static void
amsel_preferences_close_dialog (GtkDialog *dialog,
                                gpointer   user_data)
{
  amsel_preferences_close (NULL, dialog);
}

static void
amsel_preferences_refresh_interval_changed (GtkSpinButton *spin_btn,
                                            gpointer       user_data)
{
  GtkAdjustment *adj;
  AmselApplication *app = AMSEL_APPLICATION (g_application_get_default ());
  AmselEngine *engine = amsel_application_get_engine (app);
  g_autofree gchar *value;
  adj = gtk_spin_button_get_adjustment (spin_btn);
  gdouble refresh_interval = gtk_adjustment_get_value (adj);

  value = g_strdup_printf ("%f", refresh_interval);

  amsel_engine_set_prop (engine, "refresh-interval", value);
  amsel_application_setup_refresh (app);
}

static void
amsel_preferences_channel_fetched (AmselEngine  *engine,
                                   AmselChannel *channel,
                                   gpointer      user_data)
{
  AmselPreferences *self = AMSEL_PREFERENCES (user_data);

  g_list_store_append (self->channel_store, channel);
}

static void
amsel_preferences_import_opml (GtkButton *btn,
                               gpointer   user_data)
{
  AmselPreferences *self = AMSEL_PREFERENCES (user_data);
  g_autoptr(GtkFileChooserNative) native;
  GtkFileFilter *filter;
  GtkFileChooser *chooser;
  AmselEngine *engine;
  GApplication *app = g_application_get_default ();

  engine = amsel_application_get_engine (AMSEL_APPLICATION (app));
  GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
  gint res;

  native = gtk_file_chooser_native_new ("Select OPML file",
                                        GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (btn))),
                                        action,
                                        "Select",
                                        "Cancel");
  chooser =  GTK_FILE_CHOOSER (native);
  filter = gtk_file_filter_new ();
  gtk_file_filter_add_pattern (filter, "*.opml");
  gtk_file_filter_add_pattern (filter, "*.xml");
  gtk_file_chooser_add_filter (chooser, filter);

  res = gtk_native_dialog_run (GTK_NATIVE_DIALOG (native));
  if (res == GTK_RESPONSE_ACCEPT)
    {
      gchar *filename;
      gchar *contents;
      filename = gtk_file_chooser_get_filename (chooser);
      g_debug ("opml: %s", filename);
      if (g_file_get_contents (filename, &contents, NULL, NULL))
        {
          GList *urls = amsel_opml_parse (self->opmlparser, contents);

          for (; urls; urls = g_list_next (urls))
            {
              amsel_engine_fetch (engine, urls->data, "local");
            }
        }

    }

}

AmselPreferences *
amsel_preferences_new (GtkWindow *window)
{
  return g_object_new (AMSEL_TYPE_PREFERENCES,
                       "transient-for", window,
                       "modal", TRUE,
                       "use-header-bar", 1,
                       NULL);
}

static void
amsel_preferences_finalize (GObject *object)
{
  AmselPreferences *self = (AmselPreferences *)object;

  g_clear_pointer (&self->opmlparser, g_object_unref);

  G_OBJECT_CLASS (amsel_preferences_parent_class)->finalize (object);
}

static void
amsel_preferences_class_init (AmselPreferencesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = amsel_preferences_finalize;

  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Amsel/preferences.ui");
  gtk_widget_class_bind_template_child (widget_class, AmselPreferences, channels);
  gtk_widget_class_bind_template_child (widget_class, AmselPreferences, refresh_spin_btn);
  gtk_widget_class_bind_template_callback (widget_class, amsel_preferences_close);
  gtk_widget_class_bind_template_callback (widget_class, amsel_preferences_close_dialog);
  gtk_widget_class_bind_template_callback (widget_class, amsel_preferences_import_opml);
}

static void
amsel_preferences_init (AmselPreferences *self)
{
  GList *channels = NULL;
  gtk_widget_init_template (GTK_WIDGET (self));

  self->channel_store = g_list_store_new (AMSEL_TYPE_CHANNEL);

  GApplication *app = g_application_get_default ();
  AmselApplication *amsel_app = AMSEL_APPLICATION (app);

  AmselEngine *engine = amsel_application_get_engine (amsel_app);
  // TODO: potential candidate for optimization
  channels = amsel_engine_get_all_channels (engine);

  for(; channels; channels = g_list_next (channels))
    {
      g_list_store_append (self->channel_store, AMSEL_CHANNEL (channels->data));
    }

  g_list_free_full (channels, g_object_unref);

  gtk_list_box_bind_model (self->channels,
                           G_LIST_MODEL (self->channel_store),
                           amsel_preferences_create_channel_row,
                           self,
                           NULL);

  g_signal_connect (engine, "channel-fetched", G_CALLBACK (amsel_preferences_channel_fetched), self);

  g_signal_connect (self->refresh_spin_btn, "value-changed", G_CALLBACK (amsel_preferences_refresh_interval_changed), self);

  gchar *refresh_interval_str = amsel_engine_get_prop (engine, "refresh-interval");
  gdouble refresh_interval = atof (refresh_interval_str);
  GtkAdjustment *refresh_adj = gtk_spin_button_get_adjustment (self->refresh_spin_btn);

  gtk_adjustment_set_value (refresh_adj, refresh_interval);

  self->opmlparser = amsel_opml_new ();
}
