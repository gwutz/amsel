/* amsel-engine.h
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include <glib-object.h>
#include <amsel-feed-provider-manager.h>

G_BEGIN_DECLS

#define AMSEL_TYPE_ENGINE (amsel_engine_get_type())

G_DECLARE_FINAL_TYPE (AmselEngine, amsel_engine, AMSEL, ENGINE, GObject)

AmselEngine              *amsel_engine_new                              (void);
void                      amsel_engine_fetch                            (AmselEngine *self,
                                                                         const gchar *url,
                                                                         const gchar *plugin);
void                      amsel_engine_refresh_all                      (AmselEngine *self);
GList                    *amsel_engine_get_all_channels                 (AmselEngine *self);
GList                    *amsel_engine_get_all_entries                  (AmselEngine *self);
gint                      amsel_engine_n_channels                       (AmselEngine *self);
void                      amsel_engine_mark_read                        (AmselEngine *self,
                                                                         AmselEntry  *entry,
                                                                         gboolean     read);
GList                    *amsel_engine_delete_channel                   (AmselEngine  *self,
                                                                         AmselChannel *channel);
void                      amsel_engine_set_prop                         (AmselEngine *self,
                                                                         gchar       *name,
                                                                         gchar       *value);
gchar                    *amsel_engine_get_prop                         (AmselEngine *self,
                                                                         gchar       *name);

AmselFeedProviderManager *amsel_engine_get_amsel_feed_provider_manager  (AmselEngine *self);

G_END_DECLS
