/* amsel-util.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */
#include <amsel-util.h>
#include <libxml/HTMLparser.h>

void
html_characters_cb (void          *ctx,
                    const xmlChar *ch,
                    int            len)
{
  GString *buffer = (GString *)ctx;

  g_string_append_len (buffer, (const gchar *)ch, len);
}

gchar *
amsel_unhtmlize (const gchar *string)
{
  htmlParserCtxtPtr ctxt;
  htmlSAXHandlerPtr sax_parser;
  GString *buffer = g_string_new ("");

  sax_parser = g_new0 (htmlSAXHandler, 1);
  sax_parser->characters = html_characters_cb;
  ctxt = htmlCreatePushParserCtxt (sax_parser, buffer, string, strlen (string), "", XML_CHAR_ENCODING_UTF8);
  htmlParseChunk (ctxt, string, 0, 1);
  htmlFreeParserCtxt (ctxt);
  g_free (sax_parser);

  return buffer->str;
}
