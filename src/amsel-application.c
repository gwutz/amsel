#define G_LOG_DOMAIN "amsel-application"

#include "amsel-application.h"

#include "amsel-feed-provider.h"
#include "amsel-engine.h"
#include "amsel-debug.h"
#include "amsel-preferences.h"
#include "amsel-application-window.h"

struct _AmselApplication
{
  GtkApplication parent_instance;

  AmselEngine *engine;
  AmselStore *store;
  GtkApplicationWindow *window;
  guint refresh_source;
};

G_DEFINE_TYPE (AmselApplication, amsel_application, GTK_TYPE_APPLICATION)

static void
amsel_application_refresh (GSimpleAction *action,
                           GVariant      *parameter,
                           gpointer       user_data)
{
  AmselApplication *self = AMSEL_APPLICATION (user_data);
  AmselEngine *engine;
  g_debug ("%s", "Refreshing feeds...");

  engine = amsel_application_get_engine (self);
  amsel_engine_refresh_all (engine);
}

static gboolean
amsel_application_refresh_source (gpointer user_data)
{
  AmselApplication *self = AMSEL_APPLICATION (user_data);
  amsel_application_refresh (NULL, NULL, self);
  return G_SOURCE_CONTINUE;
}

static void
amsel_application_preferences (GSimpleAction *action,
                               GVariant      *parameter,
                               gpointer       user_data)
{
  AmselApplication *self = AMSEL_APPLICATION (user_data);

  GtkWindow *window = gtk_application_get_active_window (GTK_APPLICATION (self));
  if (!window)
    return;

  AmselPreferences *pref = amsel_preferences_new (window);

  gtk_dialog_run (GTK_DIALOG (pref));
}

static void
amsel_application_quit (GSimpleAction *action,
                        GVariant      *parameter,
                        gpointer       user_data)
{
  AmselApplication *self = AMSEL_APPLICATION (user_data);
  GList *list;
  list = gtk_application_get_windows (GTK_APPLICATION (self));

  while (list) {
    gtk_widget_destroy (GTK_WIDGET (list->data));
    list = g_list_next (list);
  }
}

void
amsel_application_setup_refresh (AmselApplication *self)
{
  AmselEngine *engine;

  engine = amsel_application_get_engine (self);

  if (self->refresh_source != 0)
    {
      GSource *source = g_main_context_find_source_by_id (NULL, self->refresh_source);
      g_source_destroy (source);
    }

  gchar *refresh_interval_str = amsel_engine_get_prop (engine, "refresh-interval");
  gint refresh_interval = atoi (refresh_interval_str) * 60;
  self->refresh_source = g_timeout_add_seconds (refresh_interval, amsel_application_refresh_source, self);
}

static GActionEntry app_entries[] = {
    { "refresh", amsel_application_refresh, NULL, NULL, NULL},
    { "preferences", amsel_application_preferences, NULL, NULL, NULL},
    { "quit", amsel_application_quit, NULL, NULL, NULL},
};

AmselApplication *
amsel_application_new (gchar *application_id, GApplicationFlags flags)
{
  return g_object_new (AMSEL_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
amsel_application_finalize (GObject *object)
{
  AmselApplication *self = (AmselApplication *)object;

  g_clear_object (&self->engine);

  G_OBJECT_CLASS (amsel_application_parent_class)->finalize (object);
}

static void
amsel_application_activate (GApplication *app)
{
  g_debug ("Application activate");
  AmselApplication *self = AMSEL_APPLICATION (app);

  if (!self->window) {
    self->window = GTK_APPLICATION_WINDOW (amsel_application_window_new (GTK_APPLICATION (app)));
  }

  gtk_window_present (GTK_WINDOW (self->window));
}

static void
amsel_application_startup (GApplication *app)
{
  AmselApplication *self = AMSEL_APPLICATION (app);
  g_autoptr (GtkBuilder) builder;
  GMenuModel *appmenu;

  G_APPLICATION_CLASS (amsel_application_parent_class)->startup (app);
  amsel_application_get_engine (self);

  builder = gtk_builder_new ();
  gtk_builder_add_from_resource (builder, "/org/gnome/Amsel/appmenu.ui", NULL);

  appmenu = (GMenuModel *) gtk_builder_get_object (builder, "appmenu");

  gtk_application_set_app_menu (GTK_APPLICATION (app), appmenu);

  amsel_application_setup_refresh (self);
}

static void
amsel_application_class_init (AmselApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = amsel_application_finalize;

  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);
  app_class->activate = amsel_application_activate;
  app_class->startup = amsel_application_startup;
}

static void
amsel_application_item_fetched (AmselEngine *engine,
                                gint         new_items_count,
                                gpointer     user_data)
{
  GApplication *self = G_APPLICATION (user_data);
  GNotification *notification = g_notification_new ("Amsel - new entries retrieved");

  g_autofree gchar *bodymessage = g_strdup_printf ("There are %d new entries", new_items_count);
  g_notification_set_body (notification, bodymessage);

  g_application_send_notification (self, "amselnotification", notification);
}

static void
amsel_application_init (AmselApplication *self)
{
  self->engine = amsel_engine_new ();
  self->store = amsel_store_new (self->engine);

  g_autoptr (GtkCssProvider) css_provider = gtk_css_provider_new ();
  gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
                                             GTK_STYLE_PROVIDER (css_provider),
                                             GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  gtk_css_provider_load_from_resource (css_provider, "/org/gnome/Amsel/amsel.css");

  g_action_map_add_action_entries (G_ACTION_MAP (self), app_entries, G_N_ELEMENTS (app_entries), self);

  g_signal_connect (self->engine, "new-items", G_CALLBACK (amsel_application_item_fetched), self);
}

/**
 * amsel_application_get_data_manager:
 * @self: a #AmselApplication
 *
 * Returns the #AmselDataManager
 *
 * Returns: (transfer none): the #AmselDataManager
 */
/* AmselDataManager * */
/* amsel_application_get_data_manager (AmselApplication *self) */
/* { */
/*   g_return_val_if_fail (AMSEL_IS_APPLICATION (self), NULL); */

/*   return self->data_manager; */
/* } */

/**
 * amsel_application_get_engine:
 * @self: a #AmselApplication
 *
 * Returns the #AmselEngine
 *
 * Returns: (transfer none): the #AmselEngine
 */
AmselEngine *
amsel_application_get_engine (AmselApplication *self)
{
  g_return_val_if_fail (AMSEL_IS_APPLICATION (self), NULL);

  return self->engine;
}

/**
 * amsel_application_get_window:
 * @self: a #AmselApplication
 *
 * retrieve the application window
 *
 * Returns: the #GtkApplicationWindow
 */
GtkApplicationWindow *
amsel_application_get_window (AmselApplication *self)
{
  g_return_val_if_fail (AMSEL_IS_APPLICATION (self), NULL);

  return self->window;
}

AmselStore *
amsel_application_get_store (AmselApplication *self)
{
  g_return_val_if_fail (AMSEL_IS_APPLICATION (self), NULL);

  return self->store;
}
