/* amsel-debug.h
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

#ifndef AMSEL_ENABLE_TRACE
# define AMSEL_ENABLE_TRACE @ENABLE_TRACING@
#endif

#if AMSEL_ENABLE_TRACE != 1
# undef AMSEL_ENABLE_TRACE
#endif

#ifndef AMSEL_LOG_LEVEL_TRACE
# define AMSEL_LOG_LEVEL_TRACE ((GLogLevelFlags)(1 << G_LOG_LEVEL_USER_SHIFT))
#endif

#ifdef AMSEL_ENABLE_TRACE
# define AMSEL_PROBE                                                       \
   g_log(G_LOG_DOMAIN, AMSEL_LOG_LEVEL_TRACE, "PROBE: %s():%d",            \
         G_STRFUNC, __LINE__)
#else
# define AMSEL_PROBE G_STMT_START {  } G_STMT_END
#endif

void amsel_debug(gchar *domain, gchar *message);

G_END_DECLS

