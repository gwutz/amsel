/* amsel-store.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "amsel-store.h"
#include <gio/gio.h>
#include <libsoup/soup.h>
#include "amsel-util.h"
#include "amsel-debug.h"
#include "amsel-application-window.h"
#include "amsel-application.h"
#include "amsel-date.h"

struct _AmselStore
{
  GObject parent_instance;

  GtkWidget *mainbox;
  GTree *tree;
  GListStore *liststore;
};

G_DEFINE_TYPE (AmselStore, amsel_store, G_TYPE_OBJECT)

static void
_set_read_class (GtkWidget *container)
{
  GList *children = gtk_container_get_children (GTK_CONTAINER (container));
  for (; children; children = g_list_next (children)) {
    if (GTK_IS_CONTAINER (children->data))
      _set_read_class (children->data);

    GtkStyleContext *context = gtk_widget_get_style_context (children->data);
    if (gtk_style_context_has_class (context, "unread"))
      gtk_style_context_remove_class (context, "unread");
  }
}


static void
amsel_store_article_clicked (GtkFlowBox      *flowbox,
                             GtkFlowBoxChild *child,
                             gpointer         user_data)
{
  GListStore *model = G_LIST_STORE (user_data);
  AmselApplication *app = AMSEL_APPLICATION (g_application_get_default ());
  AmselApplicationWindow *window = AMSEL_APPLICATION_WINDOW (amsel_application_get_window (app));

  gint index = gtk_flow_box_child_get_index (child);
  AmselEntry *entry = g_list_model_get_item (G_LIST_MODEL (model), index);
  amsel_application_window_show_article (window, entry);
  _set_read_class (GTK_WIDGET (child));
}

static void
amsel_store_image_loaded (GObject      *source_object,
                          GAsyncResult *res,
                          gpointer      user_data)
{
  GtkBox *box = GTK_BOX (user_data);
  GdkPixbuf *pixbuf = gdk_pixbuf_new_from_stream_finish (res, NULL);

  GtkWidget *preview_image = gtk_image_new_from_pixbuf (pixbuf);
  gtk_widget_set_halign (preview_image, GTK_ALIGN_CENTER);
  gtk_widget_show (preview_image);
  gtk_box_pack_start (GTK_BOX(box), preview_image, FALSE, FALSE, 0);
}

static void
amsel_store_image_fetched (GObject      *source_object,
                           GAsyncResult *res,
                           gpointer      user_data)
{
  GError *error = NULL;
  SoupSession *session = SOUP_SESSION (source_object);
  GtkBox *box = GTK_BOX (user_data);

  GInputStream *gis = soup_session_send_finish (session, res, &error);
  gdk_pixbuf_new_from_stream_at_scale_async (G_INPUT_STREAM (gis), 200, 150, TRUE, NULL, amsel_store_image_loaded, box);
}

static GtkWidget *
amsel_store_create_widget (gpointer item,
                           gpointer user_data)
{
  const gchar *title;
  AmselEntry *entry = AMSEL_ENTRY (item);

  GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 10);
  GtkStyleContext *context = gtk_widget_get_style_context (box);
  gtk_style_context_add_class (context, "articlebox");
  // title
  title = amsel_entry_get_title (entry);

  GtkWidget *title_lbl = gtk_label_new (title);
  context = gtk_widget_get_style_context (title_lbl);
  gtk_style_context_add_class (context, "entrytitle");
  if (!amsel_entry_get_read (entry))
    gtk_style_context_add_class (context, "unread");
  else
    gtk_style_context_remove_class (context, "unread");
  gtk_label_set_lines (GTK_LABEL (title_lbl), 2);
  gtk_label_set_line_wrap (GTK_LABEL (title_lbl), TRUE);
  gtk_label_set_ellipsize (GTK_LABEL (title_lbl), PANGO_ELLIPSIZE_END);
  gtk_widget_set_halign (title_lbl, GTK_ALIGN_FILL);
  gtk_label_set_xalign (GTK_LABEL (title_lbl), 0);
  gtk_label_set_max_width_chars (GTK_LABEL (title_lbl), 20);
  gtk_widget_show (title_lbl);
  gtk_box_pack_start (GTK_BOX(box), title_lbl, FALSE, FALSE, 0);

  // preview image
  const gchar *url = amsel_entry_get_preview_image (entry);
  if (url != NULL) {
    g_autoptr(SoupSession) session = soup_session_new ();
    g_autoptr(SoupMessage) msg = soup_message_new ("GET", url);

    if (msg != NULL)
      soup_session_send_async (session, msg, NULL, amsel_store_image_fetched, box);

  } else { // preview text
    const gchar *content = amsel_entry_get_content (entry);
    g_autofree gchar *content_real = amsel_unhtmlize (content);
    g_strdelimit (content_real, "\r\n\t", ' ');

    GtkWidget *preview_lbl = gtk_label_new (content_real);
    context = gtk_widget_get_style_context (preview_lbl);
    gtk_style_context_add_class (context, "preview_lbl");
    gtk_label_set_line_wrap (GTK_LABEL (preview_lbl), TRUE);
    gtk_label_set_line_wrap_mode (GTK_LABEL (preview_lbl), PANGO_WRAP_WORD_CHAR);
    gtk_label_set_ellipsize (GTK_LABEL (preview_lbl), PANGO_ELLIPSIZE_END);
    gtk_label_set_justify (GTK_LABEL (preview_lbl), GTK_JUSTIFY_FILL);
    gtk_label_set_lines (GTK_LABEL (preview_lbl), 8);
    gtk_widget_set_halign (preview_lbl, GTK_ALIGN_FILL);
    gtk_label_set_xalign (GTK_LABEL (preview_lbl), 0);
    gtk_label_set_max_width_chars (GTK_LABEL (preview_lbl), 30);
    gtk_widget_show (preview_lbl);
    gtk_box_pack_start (GTK_BOX(box), preview_lbl, FALSE, FALSE, 0);
  }

  gtk_widget_set_size_request (box, 260, -1);
  gtk_widget_show_all (box);
  return box;
}

GtkWidget *
amsel_store_create_list_widget (gpointer item,
                                gpointer user_data)
{
  AmselStore *self = AMSEL_STORE (user_data);
  gchar buffer[100];
  AmselDate *amseldate = AMSEL_DATE (item);
  GDateTime *now = g_date_time_new_now_local ();
  GDate *datetoday = g_date_new_dmy (g_date_time_get_day_of_month (now), g_date_time_get_month (now), g_date_time_get_year (now));
  GDate *date = amsel_date_get_date (amseldate);
  GtkWidget *new_day_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  GListStore *entries_for_date = g_list_store_new (AMSEL_TYPE_ENTRY);
  gint days = g_date_days_between (datetoday, date);
  if (days == 0) {
    g_snprintf (buffer, 100, "%s", "Today");
  } else if (days == 1) {
    g_snprintf (buffer, 100, "%s", "Yesterday");
  } else {
    g_date_strftime (buffer, 100, "%d.%m.%Y", date);
  }
  GtkWidget *lbl = gtk_label_new (buffer);
  GtkStyleContext *context = gtk_widget_get_style_context (lbl);
  gtk_style_context_add_class (context, "dateheader");
  gtk_label_set_xalign (GTK_LABEL (lbl), 0.f);
  gtk_box_pack_start (GTK_BOX (new_day_box), lbl, FALSE, FALSE, 0);
  GtkWidget *flowbox = gtk_flow_box_new ();
  gtk_flow_box_set_homogeneous (GTK_FLOW_BOX (flowbox), TRUE);
  gtk_flow_box_bind_model (GTK_FLOW_BOX (flowbox), G_LIST_MODEL (entries_for_date), amsel_store_create_widget, NULL, NULL);
  g_signal_connect (flowbox, "child-activated", G_CALLBACK (amsel_store_article_clicked), entries_for_date);

  gtk_box_pack_start (GTK_BOX (new_day_box), flowbox, FALSE, FALSE, 0);
  gtk_widget_show_all (new_day_box);
  g_tree_insert (self->tree, date, entries_for_date);
  return new_day_box;
}

static void
amsel_store_item_fetched (AmselEngine *engine,
                          AmselEntry  *entry,
                          gpointer     user_data)
{
  AmselStore *self = AMSEL_STORE (user_data);

  amsel_store_append (self, entry);
}

AmselStore *
amsel_store_new (AmselEngine *engine)
{
  AmselStore *self = g_object_new (AMSEL_TYPE_STORE, NULL);
  g_signal_connect (engine, "item-fetched", G_CALLBACK (amsel_store_item_fetched), self);
  return self;
}

static void
amsel_store_finalize (GObject *object)
{
  /* AmselStore *self = (AmselStore *)object; */

  G_OBJECT_CLASS (amsel_store_parent_class)->finalize (object);
}


static void
amsel_store_class_init (AmselStoreClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = amsel_store_finalize;
}

static gint
amsel_store_insert_sorted (gconstpointer a,
                           gconstpointer b,
                           gpointer      user_data)
{
  const GDate *date_a = (const GDate *)a;
  const GDate *date_b = (const GDate *)b;

  return g_date_compare (date_a, date_b);
}

static gint
amsel_store_date_compare (gconstpointer a,
                          gconstpointer b,
                          gpointer      user_data)
{
  const AmselDate *amseldate_a = (const AmselDate *) a;
  const AmselDate *amseldate_b = (const AmselDate *) b;
  GDate *date_a = amsel_date_get_date (amseldate_a);
  GDate *date_b = amsel_date_get_date (amseldate_b);

  return g_date_compare (date_b, date_a);
}

static void
amsel_store_init (AmselStore *self)
{
  /* AmselEngine *engine = amsel_application_get_engine (app); */
  /* g_signal_connect (engine, "item-fetched", G_CALLBACK (amsel_store_item_fetched), self); */
  self->tree = g_tree_new_full (amsel_store_insert_sorted, NULL,
                                (void (*)(gpointer))g_date_free, g_object_unref);
  self->liststore = g_list_store_new (AMSEL_TYPE_DATE);
  self->mainbox = gtk_list_box_new ();
  gtk_list_box_set_selection_mode (GTK_LIST_BOX (self->mainbox), GTK_SELECTION_NONE);
  gtk_list_box_bind_model (GTK_LIST_BOX (self->mainbox), G_LIST_MODEL (self->liststore), amsel_store_create_list_widget, self, NULL);
  gtk_widget_show (self->mainbox);
  GtkStyleContext *context = gtk_widget_get_style_context (self->mainbox);
  gtk_style_context_add_class (context, "list");
}

void
amsel_store_foreach (AmselStore    *self,
                     GTraverseFunc  func)
{
  g_return_if_fail (AMSEL_IS_STORE (self));

  g_tree_foreach (self->tree, func, self);
}

void
amsel_store_append (AmselStore *self,
                    AmselEntry *entry)
{
  g_return_if_fail (AMSEL_IS_STORE (self));

  GDateTime *datetime = amsel_entry_get_updated (entry);
  GDate *date = g_date_new_dmy (g_date_time_get_day_of_month (datetime),
                                g_date_time_get_month (datetime),
                                g_date_time_get_year (datetime));
  g_date_time_unref (datetime);

  GListStore *entries_for_date = g_tree_lookup (self->tree, date);
  if (entries_for_date != NULL) {
    g_date_free (date);
    g_list_store_append (entries_for_date, entry);
  } else {
    // generate a new gui for element
    AmselDate *amseldate = amsel_date_new (date);
    g_list_store_insert_sorted (self->liststore, amseldate, amsel_store_date_compare, self);
    entries_for_date = g_tree_lookup (self->tree, date);
    g_list_store_append (entries_for_date, entry);
  }
}

void
amsel_store_remove (AmselStore *self,
                    AmselEntry *entry)
{
  g_return_if_fail (AMSEL_IS_STORE (self));

  GDateTime *datetime = amsel_entry_get_updated (entry);
  GDate *date = g_date_new_dmy (g_date_time_get_day_of_month (datetime),
                                g_date_time_get_month (datetime),
                                g_date_time_get_year (datetime));
  g_date_time_unref (datetime);

  GListStore *entries_for_date = g_tree_lookup (self->tree, date);
  guint num_items = g_list_model_get_n_items (G_LIST_MODEL (entries_for_date));
  for (int i = 0; i < num_items; i++)
    {
      AmselEntry *item = g_list_model_get_item (G_LIST_MODEL (entries_for_date), i);
      const gchar *id_a = amsel_entry_get_id (entry);
      const gchar *id_b = amsel_entry_get_id (item);
      if (g_strcmp0 (id_a, id_b) == 0)
        {
          g_list_store_remove (entries_for_date, i);
          if (num_items == 1)
            {
              // find pos for empty row
              guint num_rows = g_list_model_get_n_items (G_LIST_MODEL (self->liststore));
              for (int j = 0; j < num_rows; j++)
                {
                  AMSEL_PROBE;
                  AmselDate *amseldate = g_list_model_get_item (G_LIST_MODEL (self->liststore), j);
                  if (g_date_compare (amsel_date_get_date (amseldate), date) == 0) {
                    g_list_store_remove (self->liststore, j);
                    break;
                  }
                }
            }
          return;
        }
    }
}

GtkWidget *
amsel_store_get_mainbox (AmselStore *self)
{
  g_return_val_if_fail (AMSEL_IS_STORE (self), NULL);

  return self->mainbox;
}
