
#include <amsel-debug.h>

void amsel_debug(gchar *domain, gchar *message)
{
  g_log (domain, G_LOG_LEVEL_DEBUG, "%s", message);
}
