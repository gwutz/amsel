/* amsel-test.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "amsel-feed-provider"

#include "amsel-feed-provider.h"

/**
 * SECTION:amsel-feed-provider
 * @title: AmselFeedProvider
 *
 * This interface is the heart of libamsel. It is essentiell the provider
 * for #AmselChannel and their corresponding #AmselEntry objects. The interface
 * itself is super simplistic. A method to load the plugins and do setup things
 * and a method to retrieve feeds on demand.
 *
 * The idea is that a source to fetch RSS and ATOM feeds can be nowadays on
 * different places, for example there is [feedly](https://www.feedly.com) or
 * you fetch RSS/ATOM directly.
 * I provide a proof-of-concept to fetch them in the old
 * fashioned way and directly download the feed and parse them to model objects.
 *
 * One example implementation in python could look like that:
 * |[<!-- language="python" -->
 *
 * class Feedparser(GObject.Object, Amsel.FeedProvider):
 *
 *      def do_load(self):
 *          # setup plugin
 *
 *      def do_retrieve(self, url):
 *          # fetch document and parse it
 * ]|
 *
 * Please mind that this API is not stable at the moment. I will implement a
 * online provider to see where i still need possible additions to this interface.
 * I think about online login possibilities for example.
 */

/**
 * AmselFeedProviderInterface:
 * @parent: parent #GTypeInterface
 * @load: This vfunc gets called when the module is loaded. The purpose is to init the module.
 * @retrieve: This vfunc fetches the feed.
 * @get_name: This vfunc describes the human readable name of the plugin
 * @get_icon: This vfunc is the graphical representation of the plugin
 */

G_DEFINE_INTERFACE (AmselFeedProvider, amsel_feed_provider, G_TYPE_OBJECT)

static void
amsel_feed_provider_default_init (AmselFeedProviderInterface *iface)
{
  iface->load = amsel_feed_provider_load;
  iface->retrieve = amsel_feed_provider_retrieve;
  iface->get_name = amsel_feed_provider_get_name;
  iface->get_icon = amsel_feed_provider_get_icon;
}

/**
 * amsel_feed_provider_load:
 * @self: the #AmselFeedProvider
 *
 * This should setup the provider plugin like initialize the system and
 * login to online provider.
 */
void
amsel_feed_provider_load (AmselFeedProvider *self)
{
  AMSEL_FEED_PROVIDER_GET_IFACE (self)->load (self);
}

/**
 * amsel_feed_provider_retrieve:
 * @self: a #AmselFeedProvider
 * @url: the address of the feed
 *
 * This method retrieves the data from the web and generates the
 * #AmselChannel
 *
 * Returns: (transfer full): the #AmselChannel
 */
AmselChannel *
amsel_feed_provider_retrieve (AmselFeedProvider *self,
                              const gchar       *url)
{
  return AMSEL_FEED_PROVIDER_GET_IFACE (self)->retrieve (self, url);
}

static void
amsel_feed_provider_retrieve_cb (GTask        *task,
                                 gpointer      source_object,
                                 gpointer      task_data,
                                 GCancellable *cancellable)
{
  AmselChannel *channel;
  AmselFeedProvider *provider = AMSEL_FEED_PROVIDER (source_object);
  gchar *url = task_data;
  g_debug ("Retrieving url async: %s", url);

  channel = amsel_feed_provider_retrieve (provider, url);

  if (channel) {
    g_task_return_pointer (task, channel, g_object_unref);
  }
}

void
amsel_feed_provider_retrieve_async (AmselFeedProvider   *self,
                                    const gchar         *url,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data)
{
  g_return_if_fail (AMSEL_IS_FEED_PROVIDER (self));
  g_autoptr (GTask) task;
  g_debug ("Retrieving url: %s", url);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_task_data (task, g_strdup (url), g_free);
  g_task_run_in_thread (task, amsel_feed_provider_retrieve_cb);
}

/**
 * amsel_feed_provider_retrieve_finish:
 * @self: a #AmselFeedProvider
 * @res: the #GAsyncResult
 * @error: a #GError, or NULL to ignore
 *
 * gets the parsed #AmselChannel
 *
 * Returns: (transfer full): the parsed #AmselChannel
 */
AmselChannel *
amsel_feed_provider_retrieve_finish (AmselFeedProvider *self,
                                     GAsyncResult      *res,
                                     GError            *error)
{
  return g_task_propagate_pointer (G_TASK (res), &error);
}

const gchar *
amsel_feed_provider_get_name (AmselFeedProvider *self)
{
  g_return_val_if_fail (AMSEL_IS_FEED_PROVIDER (self), "[invalid]");

  return AMSEL_FEED_PROVIDER_GET_IFACE (self)->get_name (self);
}

/**
 * amsel_feed_provider_get_icon:
 * @self: a #AmselFeedProvider
 *
 * the icon representation of that plugin
 *
 * Returns: (transfer full): the #GdkPixbuf
 */
GdkPixbuf *
amsel_feed_provider_get_icon (AmselFeedProvider *self)
{
  g_return_val_if_fail (AMSEL_IS_FEED_PROVIDER (self), NULL);

  return AMSEL_FEED_PROVIDER_GET_IFACE (self)->get_icon (self);
}
