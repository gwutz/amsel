# Amsel - a RSS Feedreader

This is a rudimentary feedreader for the GNOME desktop. I made it because am not
super satisfied with all existing solutions and i really like the designs at
[Gnome Wiki](https://wiki.gnome.org/Design/Apps/Potential/News).

What i really like about this approach as you see it here ist the possibility to
extend the application by plugins. At the moment there is only plugin to provide
feeds and one to save them but in the future i want to extend the API to be able
to include feedly (one of my main RSS provider) or any other webservice available.

## Installation

### via Host
Clone this repo

```
# git clone https://gitlab.gnome.org/gwutz/amsel.git
```

Then compile and install it with:

```
# mkdir build && cd build
# meson ..
# ninja
# sudo ninja install
```

### via Flatpak

As soon as i know how i can provide a flatpak for easy installation i will
provide it here. There is no official release yet, so this is a beta version
but i would love to have a nightly build for user consumption.

### via Gnome Builder

If you are brave, you can clone the repository with gnome builder and start
this application in a flatpak environment.

## Screenshots

![Amsel Screenshot](data/amsel-01.png)